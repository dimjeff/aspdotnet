﻿using LoginApp.Models;
using LoginApp.Services.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LoginApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult Login(UserModel usermodel)
        {
            //return "Result: username = "+ usermodel.Username +", password = "+usermodel.Password;
            SecurityService securityService = new SecurityService();

            if (securityService.Authenticate(usermodel))
            {
                return View("LoginSuccess", usermodel);
            }
            else
            {
                return View("LoginFailure");
            }
        }
    }
}