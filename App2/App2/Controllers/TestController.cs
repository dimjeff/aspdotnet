﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App2.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestView()
        {
            //return a testview.cshtml

            return View();
        }

        public string PrintMessage()
        {
            return "this is a test controller.";
        }

        public string Welcome(string name, int numTimes=1)
        {
            return "Hello "+name+", number is "+ numTimes;
        }

        public string Play()
        {
            return "this is a Play test.";
        }
    }
}