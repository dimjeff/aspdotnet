﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Blog.Master" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Day03Blog.register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Registration</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#username").focusout(function(){
                if($("#username").val()!=""){
                    $.get("/checkusernameavailable.aspx?username="+$("#username").val()).done(function(data){
                        if(data!="true"){
                            //alert("This username has already been taken by another user.");
                            $("#isTaken").html("This username has already been taken by another user.");
                        }else{
                            $("#isTaken").html("");
                        }
                    });
                }
            });
        });
        $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            alert("Ajax error occured");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-style-10">
        <h1>
            New User Registration
        </h1>
        <div class="section"><span>1</span>Username</div>
        <div class="inner-warp">
            <label for="username">Username</label>
            <asp:TextBox ID="username" runat="server" required="required"></asp:TextBox>
            <br/>
            <span id="isTaken"></span>
        </div>

        <div class="section"><span>2</span>Email</div>
        <div class="inner-warp">
            <label for="email">Email Address<asp:TextBox ID="email" runat="server" required="required"></asp:TextBox>
            </label>
            &nbsp;
        </div>
        <div class="section"><span>3</span>Password</div>
        <div class="inner-warp">
            <label for="password">Password<asp:TextBox ID="password" TextMode="Password" runat="server" required="required"></asp:TextBox>
            </label>
            &nbsp;
            <label for="confirm_password">Confirm Password<asp:TextBox ID="conpassword" TextMode="Password" runat="server" required="required"></asp:TextBox>
            </label>
            &nbsp;
        </div>
        <div class="divcenter">
            <asp:Button runat="server" Text="Register!" ID="Button1" OnClick="Button1_Click" />
        </div>
    </div>
</asp:Content>
