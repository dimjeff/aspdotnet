﻿using BondGadgetCollection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace BondGadgetCollection.Controllers
{
    public class GadgetsController : Controller
    {
        // GET: Gadgets
        public ActionResult Index()
        {
            List<GadgetModel> gadgets = new List<GadgetModel>();

            gadgets.Add(new GadgetModel(0, "Jerry", "no desc", "no where", "no actor"));
            gadgets.Add(new GadgetModel(1, "Berry", "no Berr", "no where1", "no actor"));
            gadgets.Add(new GadgetModel(2, "Merry", "no Merr", "no where2", "no actor"));
            gadgets.Add(new GadgetModel(3, "Terry", "no Terr", "no where3", "no actor"));

            return View("Index", gadgets);
        }
    }
}