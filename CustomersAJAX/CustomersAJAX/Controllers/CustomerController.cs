﻿using CustomersAJAX.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomersAJAX.Controllers
{
    public class CustomerController : Controller
    {
        List<Customer> customers;

        public CustomerController()
        {
            customers = new List<Customer>();
            customers.Add(new Customer(0, "Sherry", 37));
            customers.Add(new Customer(1, "Berry", 27));
            customers.Add(new Customer(2, "Jerry", 17));
            customers.Add(new Customer(3, "Terry", 7));
        }

        // GET: Customer
        public ActionResult Index()
        {
            //usually get some data from database
            Tuple<List<Customer>, Customer> tuple;
            tuple = new Tuple<List<Customer>, Customer>(customers, customers[2]);

            return View("Customer", tuple);
        }

        [HttpPost]
        public ActionResult OnSelectCustomer(string CustomerNumber)
        {
            //usually get some data from database
            Tuple<List<Customer>, Customer> tuple;
            int index = Int32.Parse(CustomerNumber);
            tuple = new Tuple<List<Customer>, Customer>(customers, customers[index]);

            return PartialView("_CustomerDetails", customers[index]);
        }
    }
}