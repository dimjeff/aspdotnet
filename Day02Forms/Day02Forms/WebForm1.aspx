﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Day02Forms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            <asp:TextBox ID="tbName" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:ListBox ID="lbItem" runat="server" Width="164px">
                <asp:ListItem>Item1</asp:ListItem>
                <asp:ListItem>Item2</asp:ListItem>
            </asp:ListBox>
            <br />
            <br />
            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="rbType" Text="Male" />
            <br />
            <asp:RadioButton ID="RadioButton2" runat="server" GroupName="rbType" Text="Female" />
            <br />
            <br />
            <asp:CheckBox ID="CheckBox1" runat="server" Text="C#" />
            <br />
            <asp:CheckBox ID="CheckBox2" runat="server" Text="ASP.net" />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Submit" />
        </div>
    </form>
</body>
</html>
